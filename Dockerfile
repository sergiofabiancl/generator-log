FROM python:slim-bookworm

# Instalar herramientas
RUN apt-get update && apt-get install -y apt-transport-https gnupg curl

# Establecer el directorio de trabajo dentro del contenedor
WORKDIR /app

# Copiar los archivos de requerimientos de Python e instalar las dependencias
COPY requirements.txt /app/requirements.txt
RUN pip install --no-cache-dir -r /app/requirements.txt

# Instala dependencias de Otel
RUN pip install opentelemetry-distro opentelemetry-exporter-otlp 
RUN opentelemetry-bootstrap -a install

# Copiar tu código fuente al directorio /app dentro del contenedor
COPY *.py /app
COPY .version /app

# Necesita las siguientes variables
# ENV OTEL_SERVICE_NAME=generator-log
# ENV OTEL_TRACES_EXPORTER=console,otlp
# ENV OTEL_METRICS_EXPORTER=console
# ENV OTEL_EXPORTER_OTLP_TRACES_ENDPOINT=0.0.0.0:4317

# Expone el puerto en el que se ejecuta la aplicación Flask
EXPOSE 5000

# Comando para ejecutar la aplicación Flask
ENTRYPOINT ["python"]
CMD ["app.py"]
