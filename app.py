# These are the necessary import declarations
import datetime
import logging

from logging.config import dictConfig
from random import randint
from flask import Flask, request, json, jsonify, make_response
from os import system
from subprocess import check_output
from datetime import timezone
#from opentelemetry import trace

## Acquire a tracer
#tracer = trace.get_tracer("diceroller.tracer")
dictConfig({
    'version': 1,
    'formatters': {'default': {
        'format': '[%(asctime)s] %(levelname)s in %(module)s: %(message)s',
    }},
    'handlers': {'console': {
        'class': 'logging.StreamHandler',
        'formatter': 'default'
    }},
    'root': {
        'level': 'INFO',
        'handlers': ['console']
    }
})

app = Flask(__name__)

# logging.basicConfig()
# logging.getLogger().setLevel(logging.INFO)
# logging.getLogger().setFormatter(logging.Formatter(
#     '[%(asctime)s] %(levelname)s in %(module)s: %(message)s'
# ))

## Request Funcion Principal
@app.route('/rolldice', methods=['POST'])
def roll_dice():
    request_data = request.get_json()
    country = request.headers.get('country')
    if 'faces' in request_data:
        max_value = request_data['faces']
        resultado = roll(max_value)
        response = make_response(
            jsonify(
                roll_result = resultado,
                roll_faces = max_value,
                message = "Success",
                service = service_data
            ),
            200
        )
        app.logger.info("200 - Successfull response")
    else:
        response = make_response(
            jsonify(
                message = "Dice faces needed!",
                service = service_data
            ),
            400
        )
        app.logger.warning("400 - Missing data")
    return response

def roll(max_value):
    #with tracer.start_as_current_span("roll") as rollspan:
        res = randint(1, max_value)
        #rollspan.set_attribute("roll.value", res)
        return res

## Request de estado de salud
@app.route('/healthcheck', methods=['GET'])
def health_check():
    status = "UP"
    timestamp = str(datetime.datetime.now(timezone.utc))
    response = make_response(
        jsonify(
            status = status,
            timestamp = timestamp,
            service = service_data
        ),
        200
    )
    app.logger.info("200 - Successfull Healthcheck")
    return response

## Obtencion de informacion del servicio
def service_info():
    service_name = "generator-log"
    service_version = check_output("cat .version", shell=True, text=True)
    data_json = {"name": service_name, "version": service_version}
    return data_json

service_data = service_info()

## Arranca la aplicacion
#app.run(debug=True, host='0.0.0.0', port=8080)

if __name__ == "__main__":
    from waitress import serve
    serve(app, host="0.0.0.0", port=5000)